import random, hashlib, re
import boto3, botocore
import qrcode

from datetime import datetime

from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404
from django.urls import reverse

from .models import Url

def index(request):
    return render(request, 'urlshortener/index.html')

def _redirect(request, id):
    dynamo = boto3.resource('dynamodb', region_name="us-east-1")
    table = dynamo.Table('urls')

    url = table.query(
        IndexName = "code-index",
        KeyConditionExpression = "code = :code",
        ExpressionAttributeValues = { ':code': id },
    )

    try:
        return redirect(url['Items'][0]['url'])
    except IndexError:
        raise Http404()

def shrink(request):
    url = request.POST['url']

    if not re.match(r'^(http|https)', url, re.I):
        url = f"http://{url}"

    hash = hashlib.sha1(url.encode()).hexdigest()
    date = datetime.today()
    code = randomString()

    dynamo = boto3.resource('dynamodb', region_name="us-east-1")
    table = dynamo.Table('urls')

    try:
        table.put_item(Item= dict(
            username='null',
            hash=hash,
            code=code,
            url = url,
            created=date.isoformat()
            ),
            ConditionExpression="attribute_not_exists(#hash)",
            ExpressionAttributeNames={'#hash': 'hash'}
        )
    except botocore.exceptions.ClientError:
        url2 = table.get_item(Key=dict(
            username='null',
            hash=hash
        ))

        code = url2['Item']['code']

    result_url = reverse('urlshortener:result', args=[code])
    return redirect(result_url)

def result(request, code):
    return render(request, 'urlshortener/result.html', dict(
        url = f'http://r302.co/{code}'
    ))

def randomString():
    chars = 'abcdefghjkmnpqrstuvwxyz23456789'
    string = ''

    for _ in range(0,5):
        string += random.choice(chars)
    return string

def qr(request):
    qr = qrcode.make(request.GET['url'])
    response = HttpResponse(content_type="image/png")
    qr.save(response, 'PNG')
    return response

    # return render(request, 'urlshortener/test.html')

from django.db import models

class Url(models.Model):
    code = models.CharField(primary_key=True, max_length=10)
    url = models.URLField()
    created_at = models.DateTimeField(auto_now=True)

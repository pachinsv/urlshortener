from django.conf.urls import url

from . import views

app_name = "urlshortener"

urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^shrink$', views.shrink, name="shrink"),
    url(r'^shorturl/([0-9a-zA-Z-_]+)$', views.result, name="result"),
    url(r'qr', views.qr, name='qr'),
    url(r'([0-9a-zA-Z-_]+)', views._redirect, name="redirect")
]
